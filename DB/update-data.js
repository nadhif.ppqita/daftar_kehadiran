require('dotenv').config({path:'.env.development.local'});
const {sql} = require('@vercel/postgres');

async function updateAbsensi(id, jamPulang) {
    try {
        const updateQuery = await sql`
            UPDATE absensi
            SET jam_pulang = ${jamPulang}
            WHERE id = ${id}
        `;

        console.log('Data berhasil diupdate');
    } catch (error) {
        console.error('Error updating data:', error);
    }
}

// Contoh pemanggilan fungsi updateAbsensi
// Misalnya kita ingin mengupdate jam pulang untuk karyawan dengan ID 1 menjadi jam 17:00:00
updateAbsensi(2, '12:00:00');
