import { useRouter } from 'next/router';
import { useState } from 'react';

export default function AddAbsensi() {
  const [idKaryawan, setIdKaryawan] = useState('');
  const [jamDatang, setJamDatang] = useState('');
  const [jamPulang, setJamPulang] = useState('');
  const [keterangan, setKeterangan] = useState('');
  const [hari, setHari] = useState('');
  const [bulan, setBulan] = useState('');
  const [tahun, setTahun] = useState('');
  const router = useRouter();

  const handleSubmit = (event) => {
    event.preventDefault();
    fetch('/api/insertData', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id_karyawan: idKaryawan,
        jam_datang: jamDatang,
        jam_pulang: jamPulang,
        keterangan: keterangan,
        hari: hari,
        bulan: bulan,
        tahun: tahun,
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Gagal menambah data.');
        }
        return response.json();
      })
      .then((json) => {
        alert('Data berhasil ditambah');
        router.push('/');
      })
      .catch((error) => {
        console.error('Error saat menambah data', error.message);
        alert('Error saat menambah data: ' + error.message);
      });
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-3xl font-bold mb-6 text-center">Tambah Data Absensi</h1>
      <form onSubmit={handleSubmit} className="max-w-md mx-auto bg-white shadow-md rounded-lg p-6 space-y-4">
        <div>
          <label className="block text-sm font-medium">ID Karyawan</label>
          <input
            type="text"
            value={idKaryawan}
            onChange={(e) => setIdKaryawan(e.target.value)}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
            required
          />
        </div>
        <div>
          <label className="block text-sm font-medium">Jam Datang</label>
          <input
            type="time"
            value={jamDatang}
            onChange={(e) => setJamDatang(e.target.value)}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
            required
          />
        </div>
        <div>
          <label className="block text-sm font-medium">Keterangan</label>
          <input
            type="text"
            value={keterangan}
            onChange={(e) => setKeterangan(e.target.value)}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
          />
        </div>
        <div>
          <label className="block text-sm font-medium">Hari</label>
          <input
            type="number"
            value={hari}
            onChange={(e) => setHari(e.target.value)}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
            required
          />
        </div>
        <div>
          <label className="block text-sm font-medium">Bulan</label>
          <input
            type="number"
            value={bulan}
            onChange={(e) => setBulan(e.target.value)}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
            required
          />
        </div>
        <div>
          <label className="block text-sm font-medium">Tahun</label>
          <input
            type="number"
            value={tahun}
            onChange={(e) => setTahun(e.target.value)}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
            required
          />
        </div>
        <button type="submit" className="bg-blue-500 text-white px-4 py-2 rounded w-full">
          Tambah Data
        </button>
      </form>
    </div>
  );
}
