import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function UpdateData() {
  const [updateData, setUpdateData] = useState();

  const router = useRouter();
  const { idUpdate } = router.query;

  useEffect(() => {
    if (!idUpdate) {
      return;
    }
    fetch(`/api/getDataDetail?id=${idUpdate}`)
      .then((res) => res.json())
      .then((data) => {
        if (!data.data) {
          alert("Tidak ada data");
          router.push(`/`);
          return;
        }
        console.log(data.data);
        setUpdateData(data.data);
      })
      .catch((err) => {
        alert("Data tidak ditemukan", err.message);
      });
  }, [idUpdate, router]);

  const handleSubmit = (event) => {
    event.preventDefault();
    const id_karyawan = event.target.id_karyawan.value;
    const jam_pulang = event.target.jam_pulang.value;
    const jam_datang = event.target.jam_datang.value;
    const keterangan = event.target.keterangan.value;
    const hari = event.target.hari.value;
    const bulan = event.target.bulan.value;
    const tahun = event.target.tahun.value;

    fetch(`/api/updateAllData?id=${idUpdate}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ id: idUpdate, id_karyawan, jam_datang, jam_pulang, keterangan, hari, bulan, tahun }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        alert("Berhasil");
        router.push("/");
      })
      .catch((err) => {
        alert("Ada masalah", err.message);
        console.log(err);
      });
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-3xl font-bold mb-6 text-center">Update Data Absensi</h1>
      {updateData && (
        <form onSubmit={handleSubmit} className="max-w-md mx-auto bg-white shadow-md rounded-lg p-6 space-y-4">
          <div>
            <label className="block text-sm font-medium">Nama Karyawan:</label>
            <input
              name="id_karyawan"
              defaultValue={updateData.id_karyawan}
              className="mt-1 p-2 border border-gray-300 rounded w-full"
              required
            />
          </div>
          <div>
            <label className="block text-sm font-medium">Jam Datang:</label>
            <input
              name="jam_datang"
              defaultValue={updateData.jam_datang}
              className="mt-1 p-2 border border-gray-300 rounded w-full"
              required
            />
          </div>
          <div>
            <label className="block text-sm font-medium">Jam Pulang:</label>
            <input
              name="jam_pulang"
              defaultValue={updateData.jam_pulang}
              className="mt-1 p-2 border border-gray-300 rounded w-full"
            />
          </div>
          <div>
            <label className="block text-sm font-medium">Keterangan:</label>
            <input
              name="keterangan"
              defaultValue={updateData.keterangan}
              className="mt-1 p-2 border border-gray-300 rounded w-full"
            />
          </div>
          <div>
            <label className="block text-sm font-medium">Tanggal:</label>
            <div className="flex space-x-2">
              <input
                name="hari"
                defaultValue={updateData.hari}
                placeholder="Hari"
                className="mt-1 p-2 border border-gray-300 rounded w-full"
                required
              />
              <input
                name="bulan"
                defaultValue={updateData.bulan}
                placeholder="Bulan"
                className="mt-1 p-2 border border-gray-300 rounded w-full"
                required
              />
              <input
                name="tahun"
                defaultValue={updateData.tahun}
                placeholder="Tahun"
                className="mt-1 p-2 border border-gray-300 rounded w-full"
                required
              />
            </div>
          </div>
          <button type="submit" className="bg-blue-500 text-white px-4 py-2 rounded w-full">
            Submit
          </button>
        </form>
      )}
    </div>
  );
}
