require('dotenv').config({path:'.env.development.local'});
const {sql} = require('@vercel/postgres');

async function execute() {
    try {
        const today = new Date();
        const idKaryawan = 'isbat'; //
        const jamDatang = '11:08:00'; //
        const keterangan = 'Hadir'; //
        const hari = today.getDate();
        const bulan = today.getMonth() + 1; // Month dimulai dari 0
        const tahun = today.getFullYear();

        const rows = await sql`
            INSERT INTO absensi (id_karyawan, jam_datang, keterangan, hari, bulan, tahun)
            VALUES (${idKaryawan}, ${jamDatang}, ${keterangan}, ${hari}, ${bulan}, ${tahun})
        `;
        
        console.log(rows);
    } catch (error) {
        console.error('Error inserting data:', error);
    }
}

execute();
