import { sql } from "@vercel/postgres";

export default async function handler(req, res) {
  // cek method
  if (req.method !== "DELETE") {
    return res.status(405).json({ error: "method not allowed" });
  }

  // cek data
  let { id } = req.query;

  if (!id) {
    return res.status(400).json({ error: "id harus ada" });
  }
  const selectData = await sql`select id from absensi where id = ${id}`
  console.log(selectData);
  if (!selectData.rows.length) {
    return res.status(400).json({ error: "id harus ada" });
  }
  // delete data
  const resData = await sql`delete from absensi where id=${id}`;

  // beritahu klo success
  return res.status(200).json({ message: "deleted", data: resData });
}