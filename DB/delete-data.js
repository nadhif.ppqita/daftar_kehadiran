require('dotenv').config({path:'.env.development.local'});
const {sql} = require('@vercel/postgres');

async function execute() {
    try {
        const result = await sql`
            DELETE FROM absensi WHERE id = 3
        `;

        // Periksa apakah ada baris yang terpengaruh
        if (result.rowCount > 0) {
            console.log("Berhasil menghapus data");
        } else {
            console.log("ID yang akan dihapus tidak ditemukan");
        }
    } catch (error) {
        console.error("Error:", error);
    }
}

execute();
