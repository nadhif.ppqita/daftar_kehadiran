const { sql } = require("@vercel/postgres");

async function insertAbsensi(req, res) {
  try {
    if (req.method !== "POST") {
      return res.status(405).json({ message: "Method tidak diperbolehkan" });
    }

    const { id_karyawan, jam_datang, jam_pulang, keterangan, hari, bulan, tahun } = req.body;

    if (!id_karyawan) {
      return res.status(400).json({ message: "id_karyawan harus diisi" });
    }
    if (!jam_datang) {
      return res.status(400).json({ message: "jam_datang harus diisi" });
    }
    if (!hari) {
      return res.status(400).json({ message: "hari harus diisi" });
    }
    if (!bulan) {
      return res.status(400).json({ message: "bulan harus diisi" });
    }
    if (!tahun) {
      return res.status(400).json({ message: "tahun harus diisi" });
    }

    // Use null if jam_pulang is empty
    const jamPulangValue = jam_pulang || null;
    const keteranganValue = keterangan || '';

    const rows = await sql`
      INSERT INTO absensi (id_karyawan, jam_datang, jam_pulang, keterangan, hari, bulan, tahun)
      VALUES (${id_karyawan}, ${jam_datang}, ${jamPulangValue}, ${keteranganValue}, ${hari}, ${bulan}, ${tahun})
    `;

    res.status(200).json({ message: "Success", data: rows });
  } catch (e) {
    console.log("ADA ERROR ", e);
    return res.status(500).json({ message: "Terjadi error" });
  }
}

export default insertAbsensi;
