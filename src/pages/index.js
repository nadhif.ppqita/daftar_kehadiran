import { useRouter } from "next/router"
import { useEffect, useState } from "react"


export default function Home() {
  const router = useRouter()
  const [getData, setGetData] = useState()

  useEffect(() => {
    fetch(`/api/getData`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data.data)
        setGetData(data.data)
      })
  }, [])

  const handleDelete = (id) => {
    fetch(`/api/delData?id=${id}`, {
      method: 'DELETE',
    })
      .then((res) => res.json())
      .then(() => {
        router.reload()
      })
      .catch((err) => {
        alert('ada masalah saat delete', err.message)
      })
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Absensi Karyawan</h1>
      {getData && getData.length === 0 && <h1>Data Kosong</h1>}
      {getData === undefined && <h1>loading....</h1>}
      <div className="space-y-4">
        {getData &&
          getData.map((data, index) => {
            return (
              <div key={index} className="p-4 border rounded shadow">
                <div className="flex justify-between items-center">
                  <div>
                    <span className="font-semibold">ID:</span> {data.id}{' '}
                    <span className="font-semibold">ID Karyawan:</span>{' '}
                    {data.id_karyawan} <span className="font-semibold">Jam
                    Datang:</span> {data.jam_datang}
                  </div>
                  <div className="space-x-2">
                    <button
                      onClick={() => {
                        router.push(`/detail/${data.id}`)
                      }}
                      className="bg-blue-500 text-white px-4 py-2 rounded"
                    >
                      Detail
                    </button>
                    <button
                      onClick={() => {
                        router.push(`update/${data.id}`)
                      }}
                      className="bg-yellow-500 text-white px-4 py-2 rounded"
                    >
                      Update
                    </button>
                    <button
                      onClick={() => {
                        handleDelete(data.id)
                      }}
                      className="bg-red-500 text-white px-4 py-2 rounded"
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            )
          })}
  <div className="flex justify-center items-center h-16">
    <button
      onClick={() => {
        router.push(`/add-data`)
      }}
      className="bg-green-500 text-white px-4 py-2 rounded"
    >
      Insert Data
    </button>
</div>

      </div>
    </div>
  )
}