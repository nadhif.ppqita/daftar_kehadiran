import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function DataDetail() {
  const router = useRouter();
  const { idDetail } = router.query;
  const [getDataDetail, setGetDataDetail] = useState();

  useEffect(() => {
    if (!idDetail) {
      return;
    }
    fetch(`/api/getDataDetail?id=${idDetail}`)
      .then((res) => res.json())
      .then((data) => {
        if (!data.data) {
          alert("Tidak ada data");
          router.push(`/`);
          return;
        }
        setGetDataDetail(data.data);
        console.log(data.data);
      })
      .catch((err) => {
        alert("Data tidak ditemukan", err.message);
      });
  }, [idDetail, router]);

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-3xl font-bold mb-6 text-center">Detail Absensi Karyawan</h1>
      {getDataDetail ? (
        <div className="max-w-md mx-auto bg-white shadow-md rounded-lg p-6">
          <div className="mb-4">
            <h2 className="text-xl font-semibold">ID Karyawan:</h2>
            <p className="text-gray-700">{getDataDetail.id_karyawan}</p>
          </div>
          <div className="mb-4">
            <h2 className="text-xl font-semibold">Jam Datang:</h2>
            <p className="text-gray-700">{getDataDetail.jam_datang}</p>
          </div>
          <div className="mb-4">
            <h2 className="text-xl font-semibold">Jam Pulang:</h2>
            <p className="text-gray-700">{getDataDetail.jam_pulang}</p>
          </div>
          <div className="mb-4">
            <h2 className="text-xl font-semibold">Keterangan:</h2>
            <p className="text-gray-700">{getDataDetail.keterangan}</p>
          </div>
          <button
            onClick={() => router.push('/')}
            className="mt-4 bg-blue-500 text-white px-4 py-2 rounded"
          >
            Kembali
          </button>
        </div>
      ) : (
        <p className="text-center text-gray-500">Memuat data...</p>
      )}
    </div>
  );
}
