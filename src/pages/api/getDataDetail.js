const { sql } = require("@vercel/postgres");

export default async function handler(req,res) {
    try {
        
        if(req.method !== "GET") {
            return res.status(405).json({message:"Method tidak diperbolehkan"})
        }

        const {id} = await req.query

        const {rows} = await sql`SELECT
        id,
        id_karyawan,
        jam_datang,
        jam_pulang,
        keterangan,
        hari,
        bulan,
        tahun FROM absensi WHERE id = ${id}`

        if (rows.length === 0) {
           return res.status(404).json({message:"Gagal menemukan data.", error:true}) 
        }
        res.status(200).json({message:"Success", data:rows[0]})
    } catch(e){
        console.log("ADA ERROR ", e)
        return res.status(500).json({message:"Terjadi error,"})
    }
}