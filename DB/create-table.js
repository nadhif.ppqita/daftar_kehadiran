require('dotenv').config({path:'.env.development.local'});
const {sql} = require('@vercel/postgres');

async function execute() {
    const deleteTable = await sql`DROP TABLE IF EXISTS absensi`;

    const createTable = await sql`
    CREATE TABLE IF NOT EXISTS absensi (
       id SERIAL PRIMARY KEY,
       id_karyawan VARCHAR(20) NOT NULL,
       jam_datang TIME,
       jam_pulang TIME,
       keterangan TEXT,
       hari INTEGER,
       bulan INTEGER,
       tahun INTEGER
    )`;
    console.log(createTable);
}

execute();
