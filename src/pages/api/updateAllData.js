const { sql } = require("@vercel/postgres");

async function updateAllData(req,res) {
    try {
        
        if(req.method !== "PUT") {
            return res.status(405).json({message:"Method tidak diperbolehkan"})
        }

        const {id_karyawan, jam_pulang, jam_datang, keterangan, hari, bulan, tahun} = req.body
        console.log(req.body);
        const {id} = await req.query

        if(!jam_pulang || !id_karyawan || !jam_datang  || !hari || !bulan || !tahun) {
            return res.status(400).json({message:"Data tidak boleh kosong"})
        }

        if(!id) {
            return res.status(400).json({message:"Id tidak boleh kosong"})
        }

        const rows = await sql`UPDATE absensi SET jam_pulang = ${jam_pulang}, id_karyawan = ${id_karyawan}, keterangan = ${keterangan}, hari = ${hari}, bulan = ${bulan}, tahun = ${tahun} WHERE id = ${id}`

        res.status(200).json({message:"Success", data:rows})
    } catch(e){
        console.log("ADA ERROR ", e)
        return res.status(500).json({message:"Terjadi error,"})
    }
}

export default (updateAllData)